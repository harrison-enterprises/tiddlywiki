# TiddlyWiki Project Template

## Setup
- Install tiddlywiki: `npm install -g tiddlywiki`
- From the folder containing the project folder, run `npm run start`
- You can now view and edit the wiki at localhost:8081 in your browser
- Updates will automatically be saved to your file system, commit your changes to git when done
